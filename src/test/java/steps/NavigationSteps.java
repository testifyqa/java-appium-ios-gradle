package steps;

import io.cucumber.java.en.Given;
import pages.BasePage;
import pages.Page;

public class NavigationSteps extends Page {

    @Given("^the Artistry app user is viewing paintings by Pablo Picasso$")
    public void iAmViewingPaintingsByPabloPicasso() {
        instanceOf(BasePage.class).navToPabloPicasso();
    }
}