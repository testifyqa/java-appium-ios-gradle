package utils.appium;

import io.appium.java_client.ios.IOSDriver;

public class Driver {

    public static IOSDriver appDriver() {
        return DriverController.instance.iosDriver;
    }
}