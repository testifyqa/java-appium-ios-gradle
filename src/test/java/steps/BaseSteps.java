package steps;

import io.cucumber.java.en.Then;
import pages.BasePage;
import pages.Page;

import java.util.List;

public class BaseSteps extends Page {

    @Then("^the user sees \"([^\"]*)\" in the PageSource$")
    public void i_see_in_the_PageSource(String expectedPageSource) {
        instanceOf(BasePage.class).validatePageSource(expectedPageSource);
    }

    @Then("^the user sees$")
    public void iSee(List<String> existsInPageSource) {
        instanceOf(BasePage.class).validateMultipleInPageSource(existsInPageSource);
    }

    @Then("^the user sees the current context is \"([^\"]*)\"$")
    public void iSeeTheCurrentContextIs(String currentContext) {
        instanceOf(BasePage.class).validateCurrentContext(currentContext);
    }
}
