@Iphone8 @IphoneX
Feature: Navigation Scenarios
  As a user of Artistry, I can navigate around the Artistry app

  @noReset
  Scenario: 01. View the 'Home' tab whilst logged out
    Given the Artistry app user is viewing paintings by Pablo Picasso
    When the user views more info for Guernica painting
    Then the user sees
      | Guernica is a                |
      | mural-sized oil painting     |
      | powerful anti-war paintings  |

    Scenario: 02. test
      Given the Artistry app user is viewing paintings by Pablo Picasso
      When the user views more info for Guernica painting
