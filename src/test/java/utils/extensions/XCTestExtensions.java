package utils.extensions;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.util.HashMap;
import java.util.Map;

import static utils.appium.Driver.appDriver;

public class XCTestExtensions extends MobileElementExtensions {

    private static IOSDriver driver = appDriver();

    //Swipe methods
    public static void xcSwipe(String direction) {
        Map<String, Object> params = new HashMap<>();
        params.put("direction", direction);
        driver.executeScript("mobile: swipe", params);
    }

    public static void xcSwipe(String direction, MobileElement element) {
        meElementIsDisplayed(element);
        Map<String, Object> params = new HashMap<>();
        params.put("direction", direction);
        params.put("element", element.getId());
        driver.executeScript("mobile: swipe", params);
    }

    //Scroll methods
    public static void xcScroll(String direction) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("direction", direction);
        driver.executeScript("mobile: scroll", params);
    }

    public static void xcScroll(String direction, IOSElement element) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("direction", direction);
        params.put("element", element);
        driver.executeScript("mobile: scroll", params);
    }

    public static void xcScroll(String direction, String parameter, String nameOrPredicateString) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("direction", direction);
        params.put(parameter, nameOrPredicateString);
        driver.executeScript("mobile: scroll", params);
    }

    //PinchOrZoom methods
    public static void xcPinchOrZoom(double scale) {
        Map<String, Object> args = new HashMap<>();
        args.put("scale", scale);
        driver.executeScript("mobile: pinch", args);
    }

    public static void xcPinchOrZoom(MobileElement element, double scale) {
        meElementIsDisplayed(element);
        Map<String, Object> args = new HashMap<>();
        args.put("scale", scale);
        args.put("element", element.getId());
        driver.executeScript("mobile: pinch", args);
    }

    //Tap methods
    public static void xcTapOn(double x, double y) {
        Map<String, Object> args = new HashMap<>();
        args.put("x", x);
        args.put("y", y);
        driver.executeScript("mobile: tap", args);
    }

    public static void xcTapOn(MobileElement element, double x, double y) {
        meElementIsDisplayed(element);
        Map<String, Object> args = new HashMap<>();
        args.put("element", element.getId());
        args.put("x", x);
        args.put("y", y);
        driver.executeScript("mobile: tap", args);
    }

    //DoubleTap methods
    public static void xcDoubleTapOn(double x, double y) {
        Map<String, Object> args = new HashMap<>();
        args.put("x", x);
        args.put("y", y);
        driver.executeScript("mobile: doubleTap", args);
    }

    public static void xcDoubleTapOn(MobileElement element) {
        meElementIsDisplayed(element);
        Map<String, Object> args = new HashMap<>();
        args.put("element", element.getId());
        driver.executeScript("mobile: doubleTap", args);
    }

    //Two Finger Tap method
    public static void xcTwoFingerTapOn(MobileElement element) {
        meElementIsDisplayed(element);
        Map<String, Object> args = new HashMap<>();
        args.put("element", element.getId());
        driver.executeScript("mobile: twoFingerTap", args);
    }

    //Touch and Hold method
    public static void xcTouchAndHoldOn(MobileElement element, double duration) {
        meElementIsDisplayed(element);
        Map<String, Object> args = new HashMap<>();
        args.put("element", element.getId());
        args.put("duration", duration);
        driver.executeScript("mobile: touchAndHold", args);
    }

    //Drag from x to y for duration methods
    public static void xcDragFromToForDuration(double duration, double fromX, double fromY, double toX, double toY) {
        Map<String, Object> args = new HashMap<>();
        args.put("duration", duration);
        args.put("fromX", fromX);
        args.put("fromY", fromY);
        args.put("toX", toX);
        args.put("toY", toY);
        driver.executeScript("mobile: dragFromToForDuration", args);
    }

    public static void xcDragFromToForDuration(MobileElement element, double duration, double fromX, double fromY, double toX, double toY) {
        meElementIsDisplayed(element);
        Map<String, Object> args = new HashMap<>();
        args.put("element", element.getId());
        args.put("duration", duration);
        args.put("fromX", fromX);
        args.put("fromY", fromY);
        args.put("toX", toX);
        args.put("toY", toY);
        driver.executeScript("mobile: dragFromToForDuration", args);
    }

    //Picker wheel method
    public static void xcSelectPickerWheelValue(MobileElement element, String order, double offset) {
        Map<String, Object> params = new HashMap<>();
        params.put("order", order);
        params.put("offset", offset);
        params.put("element", element.getId());
        driver.executeScript("mobile: selectPickerWheelValue", params);
    }

    //Alert actions methods
    public static void xcPerformActionOnAlert(String action) {
        meWaitForSeconds().until(ExpectedConditions.alertIsPresent());
        HashMap<String, String> args = new HashMap<>();
        args.put("action", action);
        driver.executeScript("mobile: alert", args);
    }

    public static void xcPerformActionOnAlert(String action, String buttonLabel) {
        meWaitForSeconds().until(ExpectedConditions.alertIsPresent());
        HashMap<String, String> args = new HashMap<>();
        args.put("action", action);
        args.put("buttonLabel", buttonLabel);
        driver.executeScript("mobile: alert", args);
    }
}