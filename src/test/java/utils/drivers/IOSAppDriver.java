package utils.drivers;

import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import utils.appium.AppiumServer;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class IOSAppDriver {

    private static IOSDriver driver;

    public static IOSDriver loadDriver(String platformVersion, String deviceName, int wdaLocalPort) throws MalformedURLException {
        AppiumServer.start();

        File file = new File("src");
        File fileApp = new File(file, "Artistry.app");

        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("platformVersion", platformVersion);
        cap.setCapability("deviceName", deviceName);
        cap.setCapability("wdaLocalPort", wdaLocalPort);
        cap.setCapability("automationName", "XCUITest");
        cap.setCapability("app", fileApp.getAbsolutePath());

        driver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        return driver;
    }
}