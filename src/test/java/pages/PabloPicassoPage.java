package pages;

import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static utils.extensions.MobileElementExtensions.meTap;

public class PabloPicassoPage extends BasePage {

    private static Logger log = LogManager.getLogger(PabloPicassoPage.class.getName());

    @iOSFindBy(accessibility = "Guernica")
    private IOSElement btnGuernica;

    public void viewMoreInfoGuernica() {
        meTap(btnGuernica);
        log.info(":: We select to view more info for the 'Guernica' painting");
    }
}
