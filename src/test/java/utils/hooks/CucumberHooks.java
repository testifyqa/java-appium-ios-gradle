package utils.hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import pages.BasePage;
import utils.appium.DriverController;
import java.io.IOException;

import static pages.Page.instanceOf;

public class CucumberHooks {

    @Before("~@noReset")
    public void before() {
        instanceOf(BasePage.class).resetApp();
    }

    @Before("@DebugIphone8")
    public void beforeIphone8() throws IOException {
        DriverController.instance.startAppDriver("12.1","iPhone 8", 8101);
        instanceOf(BasePage.class).appFullyLaunched();
    }

    @Before("@DebugIphoneX")
    public void beforeIphoneX() throws IOException {
        DriverController.instance.startAppDriver("12.1", "iPhone X", 8102);
        instanceOf(BasePage.class).appFullyLaunched();
    }

    @After("@DebugIphone8, @DebugIphoneX")
    public void afterDevices() {
        DriverController.instance.stopAppDriver();
    }
}