package pages;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import java.util.List;

import static utils.appium.Driver.appDriver;
import static utils.extensions.MobileElementExtensions.*;

public class BasePage extends Page {

    private IOSDriver driver = appDriver();

    private static Logger log = LogManager.getLogger(BasePage.class.getName());

    private String getPageSource() { return driver.getPageSource(); }
    private String getCurrentContext() { return driver.getContext(); }

    @iOSFindBy(accessibility = "Pablo Picasso")
    private IOSElement btnPabloPicasso;

    public void resetApp() {
        driver.resetApp();
        appFullyLaunched();
    }

    public void appFullyLaunched() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOf(btnPabloPicasso));
    }

    public void validatePageSource(String expectedPageSource) {
        Assert.assertTrue(getPageSource().contains(expectedPageSource));
        log.info(":: The text " + expectedPageSource + " is present in the app screen's source.");
    }

    public void validateMultipleInPageSource(List<String> table) {
        for (String row : table) {
            Assert.assertTrue(getPageSource().contains(row));
            log.info("The text " + row + " is in the app screen's source.");
        }
    }

    public void validateCurrentContext(String currentContext) {
        Assert.assertTrue(getCurrentContext().contains(currentContext));
        log.info(":: The current context is: " + currentContext);
    }

    public PabloPicassoPage navToPabloPicasso() {
        meTap(btnPabloPicasso);
        log.info(":: We navigate to the Pablo Picasso screen");
        return instanceOf(PabloPicassoPage.class);
    }
}